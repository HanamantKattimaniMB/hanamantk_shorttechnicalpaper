# Improvement performance of FULL-TEXT SEARCH ##
 
Full-text search is the most advanced searching technique in the database. It works by using text indexes. we take a deeper look and compare the two leading open-source search engines built on top of Lucene. Following three are search engines for text-search.
 

1. ElasticSearch
2. Solr
3. Lucene

![ElasticSearch](https://images.contentstack.io/v3/assets/bltefdd0b53724fa2ce/blt280217a63b82a734/5bbdaacf63ed239936a7dd56/elastic-logo.svg)

![Solr](https://nsfocusglobal.com/wp-content/uploads/2019/11/1112-8.jpg)
 
 ![Lucene](https://lucene.apache.org/theme/images/lucene/lucene_logo_green_300.png)

* ## [Elastic Search :](https://www.elastic.co/)
<p>     According to an official website of Elastic Search, Elastic search is simple to access with a single sign-on.  It’s the most popular search engine and has been available since 2010. It provides document-level security. Search across all of our application content and historical workplace records.
According to information provided by elastic search nearly all operating systems like Windows, Linux, Android, etc support. According to charts provided by elastic search is input delay 3ms which is good.
 </p>
 
* #### Features:
  * Security
  * Integration
  * Management
  * Deployment
 
* ## [Solr :](https://lucene.apache.org/solr/)
 <p> Solr is highly reliable, providing indexing, replication, and load-balanced querying, centralized configuration which is an apache product. Solr has hundreds of features which make them versatile. The performance of Solr is good. Some popular search engines like DuckDuckGo,eHarmony, Sears, etc work with Solr. It is useful to search and analytics.
It also supports advance full-text search capabilities including phrases, wildcards, joins, grouping, and much more across any data type.</p>
 

 
* #### Features:
  * Advance Full-text Search Capabilities
  * Optimized for High Volume Traffic
  * Standards-Based Open Interfaces XML, JSON, and HTTP
  * Comprehensive Administration Interfaces
  * Easy Monitoring
 
* ## [Lucene :](https://lucene.apache.org/)
<p>
 Lucene also works with apache we can call ‘Apache Lucene’. Lucene core is one of the powerful concepts. Lucine Core is one of the java libraries which provides powerful indexing and search features, Latest version of Apache Lucene 8.7.0 is available. It allows simultaneous update and searching. It requires a small amount of RAM. Index size is hardly 20-30% of text indexed.

</P>
 
* #### Features:
    * Scalable, High-Performance Indexing
    * Powerful, Accurate and Efficient Search Algorithms
    * Cross-Platform Solution



* ## Relation Between ElasticSearch and Solr:


 ![ElasticVsSolr](https://i.stack.imgur.com/SPLYN.jpg)

<p>
Solr fits better into enterprise applications that already implement big data ecosystem tools, such as Hadoop and Spark. Solr and Elasticsearch both support NRT (near real-time) searches and take advantage of all of Lucene’s search capabilities. They both have additional search-related feature sets.

Solr is useful in optimal text search and enterprise applications close to the big data ecosystem and Elastic search is useful in both text search and analytical engine because of its powerful aggregation module.

If we consider the community of Solr it has a historically large ecosystem and robust ecosystem for the FOSS version and the ELK Stack.</p>
<br>
|Features|Elastic Search|Solr|
|--------------|--------|---------|
|Supported Format|JSON |XML, CSV, JSON |
|Multiple document types per schema|No|Yes|
|Pluggable search workflow|via SearchComponents|No pluggable search|
|Visualisation|Banana (Port of Kibana)|Kibana|

<br>

* ## Relation Between Solr and Lucene:
<p>Unlike Lucene, Solr is a web application (WAR) which can be deployed in any servlet container, e.g. Jetty, Tomcat, Resin, etc.
Solr can be installed and used by non-programmers. Lucene cannot.</p>

## References:

Lucene: https://lucene.apache.org/solr/

ElasticSearch: https://www.elastic.co/

Lucene: https://lucene.apache.org/
